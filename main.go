package main

import (
	"fmt"
)

func foo(bar string) string {
	return "fooo" + bar
}

func main() {
	//	fmt.Printf("Hello %s", foo("World"))
	FizzBuzz()
}

func barcamp() {
	bar := &Barcamp{
		Title: "local-it Barcamp 2022",
		Foo:   42,
	}

	bar.Title = "local-it Barcamp 2023"

	bar.Code()

	fmt.Println(bar)
}

type Barcamp struct {
	Title string
	Foo   int
}

func (b *Barcamp) Code() {
	fmt.Println("Code")
}

func (b *Barcamp) String() string {
	return b.Title
}
