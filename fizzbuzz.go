package main

import (
	"fmt"
	"strconv"
)

func FizzBuzz() {
	for i := 0; i < 20; i++ {
		fmt.Println(fizzOrBuzz(i))
	}
}

func fizzOrBuzz(number int) string {
	out := handle3(number) + handle5(number)
	if out != "" {
		return out
	}

	return strconv.Itoa(number)
}

func handle5(number int) string {
	if number%5 == 0 {
		return "Buzz"
	}
	return ""
}

func handle3(number int) string {
	if number%3 == 0 {
		return "Fizz"
	}
	return ""
}
