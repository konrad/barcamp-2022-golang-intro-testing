package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFizzBuzz(t *testing.T) {
	t.Run("number", func(t *testing.T) {
		out := fizzOrBuzz(1)
		assert.Equal(t, "1", out)
	})
	t.Run("number 2", func(t *testing.T) {
		out := fizzOrBuzz(2)
		assert.Equal(t, "2", out)
	})
	t.Run("number 3", func(t *testing.T) {
		out := fizzOrBuzz(3)
		assert.Equal(t, "Fizz", out)
	})
	t.Run("number 5", func(t *testing.T) {
		out := fizzOrBuzz(5)
		assert.Equal(t, "Buzz", out)
	})
	t.Run("number 6", func(t *testing.T) {
		out := fizzOrBuzz(6)
		assert.Equal(t, "Fizz", out)
	})
	t.Run("number 10", func(t *testing.T) {
		out := fizzOrBuzz(10)
		assert.Equal(t, "Buzz", out)
	})
	t.Run("number 15", func(t *testing.T) {
		out := fizzOrBuzz(15)
		assert.Equal(t, "FizzBuzz", out)
	})
}
